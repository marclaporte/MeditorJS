"use strict";

const os           = require("os");
const gulp         = require("gulp");
const sass         = require("gulp-sass")(require('sass'));
const jshint       = require("gulp-jshint");
const uglify       = require("gulp-uglify");
const rename       = require("gulp-rename");
const concat       = require("gulp-concat");
const header       = require("gulp-header");
const minifycss    = require("gulp-minify-css");
//const jsdoc        = require("gulp-jsdoc");
//const jsdoc2md     = require("gulp-jsdoc-to-markdown");
const pkg          = require("./package.json");
const dateFormat   = require("dateformatter").format;
const replace      = require("gulp-replace");
const babel      = require("gulp-babel");

pkg.name         = "MeditorJS";
pkg.today        = dateFormat;

let headerComment = ["/*", 
					" * <%= pkg.name %>",
                    " *",
					" * @file        <%= fileName(file) %> ",
					" * @version     v<%= pkg.version %> ",
					" * @description <%= pkg.description %>",
					" * @license     MIT License",
					" * @author      <%= pkg.author %>",
					" * {@link       <%= pkg.homepage %>}",
					" * @updateTime  <%= pkg.today('Y-m-d') %>",
					" */", 
					"\r\n"].join("\r\n");

let headerMiniComment = "/*! <%= pkg.name %> v<%= pkg.version %> | <%= fileName(file) %> | <%= pkg.description %> | MIT License | By: <%= pkg.author %> | <%= pkg.homepage %> | <%=pkg.today('Y-m-d') %> */\r\n";

function scssTask(fileName, path) {
    
    path = path || "scss/";
    
    const distPath = "dist/css";
    const minDistPath = "dist-min/css"
    
    return gulp.src(path + fileName + ".scss")
        .pipe(sass({ outputStyle: "expanded", sourceMap: false }).on("error", sass.logError))
        .pipe(header(headerComment, {pkg : pkg, fileName : function(file) { 
            let name = file.path.split(file.base);
            return name[1].replace("\\", "");
        }}))
        .pipe(gulp.dest(distPath)) 
        .pipe(rename({ suffix: ".min" }))
        .pipe(minifycss()) 
        .pipe(header(headerMiniComment, {pkg : pkg, fileName : function(file) { 
            let name = file.path.split(file.base);
            return name[1].replace("\\", "");
        }}))
        .pipe(gulp.dest(minDistPath));
};

function scss() { 
	return scssTask("editormd");
} 

function scss2() { 
	return scssTask("editormd.preview");
}

function js() { 
  return gulp.src("./src/meditor.js")
    .pipe(replace("${meditorjsVersion}", pkg.version))
    .pipe(jshint("./.jshintrc"))
    .pipe(jshint.reporter("default"))
    .pipe(header(headerComment, {pkg : pkg, fileName : function(file) { 
        let name = file.path.split(file.base);
        return name[1].replace(/[\\\/]?/, "");
    }}))
    .pipe(gulp.dest("dist/"))
    .pipe(rename({ suffix: ".min" }))
    .pipe(babel({presets: ['@babel/env']}))
    .pipe(uglify())  // {outSourceMap: true, sourceRoot: './'}	
    .pipe(header(headerMiniComment, {pkg : pkg, fileName : function(file) {
        let name = file.path.split(file.base + ( (os.platform() === "win32") ? "\\" : "/") );
        return name[1].replace(/[\\\/]?/, "");
    }}))
    .pipe(gulp.dest("dist-min/"));
}

function amd() {
    let replaceText1 = [
        'var cmModePath  = "codemirror/mode/";',
        '            var cmAddonPath = "codemirror/addon/";', 
        '',
        '            var codeMirrorModules = [',
        '                "jquery", "marked", "prettify",',
        '                "katex", "raphael", "underscore", "flowchart",  "jqueryflowchart",  "sequenceDiagram",',
        '',
        '                "codemirror/lib/codemirror",',
        '                cmModePath + "css/css",',
        '                cmModePath + "sass/sass",', 
        '                cmModePath + "shell/shell",', 
        '                cmModePath + "sql/sql",',
        '                cmModePath + "clike/clike",',
        '                cmModePath + "php/php",',
        '                cmModePath + "xml/xml",',
        '                cmModePath + "markdown/markdown",', 
        '                cmModePath + "javascript/javascript",',
        '                cmModePath + "htmlmixed/htmlmixed",',
        '                cmModePath + "gfm/gfm",',
        '                cmModePath + "http/http",',
        '                cmModePath + "go/go",', 
        '                cmModePath + "dart/dart",', 
        '                cmModePath + "coffeescript/coffeescript",',
        '                cmModePath + "nginx/nginx",',
        '                cmModePath + "python/python",', 
        '                cmModePath + "perl/perl",',
        '                cmModePath + "lua/lua",', 
        '                cmModePath + "r/r", ',
        '                cmModePath + "ruby/ruby", ',
        '                cmModePath + "rst/rst",',
        '                cmModePath + "smartymixed/smartymixed",', 
        '                cmModePath + "vb/vb",',
        '                cmModePath + "vbscript/vbscript",', 
        '                cmModePath + "velocity/velocity",',
        '                cmModePath + "xquery/xquery",',
        '                cmModePath + "yaml/yaml",',
        '                cmModePath + "erlang/erlang",', 
        '                cmModePath + "jade/jade",',
        '',
        '                cmAddonPath + "edit/trailingspace", ',
        '                cmAddonPath + "dialog/dialog", ',
        '                cmAddonPath + "search/searchcursor", ',
        '                cmAddonPath + "search/search", ',
        '                cmAddonPath + "scroll/annotatescrollbar", ', 
        '                cmAddonPath + "search/matchesonscrollbar", ',
        '                cmAddonPath + "display/placeholder", ',
        '                cmAddonPath + "edit/closetag", ',
        '                cmAddonPath + "fold/foldcode",',
        '                cmAddonPath + "fold/foldgutter",',
        '                cmAddonPath + "fold/indent-fold",',
        '                cmAddonPath + "fold/brace-fold",',
        '                cmAddonPath + "fold/xml-fold", ',
        '                cmAddonPath + "fold/markdown-fold",',
        '                cmAddonPath + "fold/comment-fold", ',
        '                cmAddonPath + "mode/overlay", ',
        '                cmAddonPath + "selection/active-line", ',
        '                cmAddonPath + "edit/closebrackets", ',
        '                cmAddonPath + "display/fullscreen",',
        '                cmAddonPath + "search/match-highlighter"',
        '            ];',
        '',
        '            define(codeMirrorModules, factory);'
    ].join("\r\n");
    
    let replaceText2 = [
        "if (typeof define == \"function\" && define.amd) {",
        "       $          = arguments[0];",
        "       marked     = arguments[1];",
        "       prettify   = arguments[2];",
        "       katex      = arguments[3];",
        "       Raphael    = arguments[4];",
        "       _          = arguments[5];",
        "       flowchart  = arguments[6];",
        "       CodeMirror = arguments[9];",
        "   }"
    ].join("\r\n");
    
    return gulp.src("src/meditor.js")
        .pipe(replace("${meditorjsVersion}", pkg.version))
        .pipe(rename({ suffix: ".amd" }))
        .pipe(header(headerComment, {pkg : pkg, fileName : function(file) { 
            let name = file.path.split(file.base);
            return name[1].replace(/[\\\/]?/, "");
        }}))
        .pipe(replace("/* Require.js define replace */", replaceText1))
        .pipe(replace("/* Require.js assignment replace */", replaceText2))
        .pipe(gulp.dest('dist/'))
        .pipe(rename({ suffix: ".min" }))
        .pipe(babel({ presets: ['@babel/env'] }))
        .pipe(uglify()) //{outSourceMap: true, sourceRoot: './'}
        .pipe(header(headerMiniComment, {pkg : pkg, fileName : function(file) {
            let name = file.path.split(file.base + ( (os.platform() === "win32") ? "\\" : "/") );
            return name[1].replace(/[\\\/]?/, "");
        }}))
        .pipe(gulp.dest("dist-min/"));
}


let codeMirror = {
    path : {
        src : {
            mode : "lib/codemirror/mode",
            addon : "lib/codemirror/addon"
        },
        dist : "dist/lib/codemirror",
        distMin: "dist-min/lib/codemirror"
    },
    modes : [
        "css",
        "sass",
        "shell",
        "sql",
        "clike",
        "php",
        "xml",
        "markdown",
        "javascript",
        "htmlmixed",
        "gfm",
        "http",
        "go",
        "dart",
        "coffeescript",
        "nginx",
        "python",
        "perl",
        "lua",
        "r", 
        "ruby", 
        "rst",
        "smartymixed",
        "vb",
        "vbscript",
        "velocity",
        "xquery",
        "yaml",
        "erlang",
        "jade",
    ],

    addons : [
        "edit/trailingspace", 
        "dialog/dialog", 
        "search/searchcursor", 
        "search/search",
        "scroll/annotatescrollbar", 
        "search/matchesonscrollbar", 
        "display/placeholder", 
        "edit/closetag", 
        "fold/foldcode",
        "fold/foldgutter",
        "fold/indent-fold",
        "fold/brace-fold",
        "fold/xml-fold", 
        "fold/markdown-fold",
        "fold/comment-fold", 
        "mode/overlay", 
        "selection/active-line", 
        "edit/closebrackets", 
        "display/fullscreen", 
        "search/match-highlighter"
    ]
};

function cmMode() { 
    
    let modes = [
        codeMirror.path.src.mode + "/meta.js"
    ];
    
    for(let i in codeMirror.modes) {
        let mode = codeMirror.modes[i];
        modes.push(codeMirror.path.src.mode + "/" + mode + "/" + mode + ".js");
    }
    
    return gulp.src(modes)
        .pipe(concat("modes.min.js"))
        .pipe(gulp.dest(codeMirror.path.dist))
        .pipe(uglify()) // {outSourceMap: true, sourceRoot: codeMirror.path.dist}
        .pipe(gulp.dest(codeMirror.path.dist))	
        .pipe(header(headerMiniComment, {pkg : pkg, fileName : function(file) {
            let name = file.path.split(file.base + "\\"); 
            return (name[1]?name[1]:name[0]).replace(/\\/g, "");
        }}))
        .pipe(gulp.dest(codeMirror.path.dist))
        .pipe(gulp.dest(codeMirror.path.distMin));
}

function cmAddon() { 
    
    let addons = [];
    
    for(let i in codeMirror.addons) {
        let addon = codeMirror.addons[i];
        addons.push(codeMirror.path.src.addon + "/" + addon + ".js");
    }
    
    return gulp.src(addons)
        .pipe(concat("addons.min.js"))
        .pipe(gulp.dest(codeMirror.path.dist))
        .pipe(uglify()) //{outSourceMap: true, sourceRoot: codeMirror.path.dist}
        .pipe(gulp.dest(codeMirror.path.dist))	
        .pipe(header(headerMiniComment, {pkg : pkg, fileName : function(file) {
            let name = file.path.split(file.base + "\\");
            return (name[1]?name[1]:name[0]).replace(/\\/g, "");
        }}))
        .pipe(gulp.dest(codeMirror.path.dist))
        .pipe(gulp.dest(codeMirror.path.distMin));
} 
/*
gulp.task("jsdoc", function(){
    return gulp.src(["./src/meditor.js", "README.md"])
               .pipe(jsdoc.parser())
               .pipe(jsdoc.generator("./docs/html"));
});

gulp.task("jsdoc2md", function() {
    return gulp.src("src/js/meditor.js")
            .pipe(jsdoc2md())
            .on("error", function(err){
                gutil.log(gutil.colors.red("jsdoc2md failed"), err.message);
            })
            .pipe(rename(function(path) {
                path.extname = ".md";
            }))
            .pipe(gulp.dest("docs/markdown"));
});
*/

function copyRessources() {
    gulp.src("./fonts/**/*").pipe(gulp.dest("dist/fonts")).pipe(gulp.dest("dist-min/fonts"));
    gulp.src("./images/**/*").pipe(gulp.dest("dist/images")).pipe(gulp.dest("dist-min/images"));
    gulp.src("./languages/**/*").pipe(gulp.dest("dist/languages")).pipe(gulp.dest("dist-min/languages"));
    gulp.src("./plugins/**/*").pipe(gulp.dest("dist/plugins")).pipe(gulp.dest("dist-min/plugins"));
    return gulp.src("./lib/**/*").pipe(gulp.dest("dist/lib")).pipe(gulp.dest("dist-min/lib"));
}
exports.watch = () => {
    gulp.watch("scss/editormd.scss", scss)
	gulp.watch("scss/editormd.preview.scss", gulp.series( scss, scss2))
	gulp.watch("scss/editormd.logo.scss", scss)
	gulp.watch("src/meditor.js", gulp.series(js, amd))
}

exports.default = gulp.series(
    copyRessources,
    gulp.parallel(
        scss,
        scss2,
        js,
        amd,
        cmAddon,
        cmMode
    )
)